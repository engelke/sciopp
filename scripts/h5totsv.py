#!/usr/bin/env python3
from h5py import File as H5File
import sys
import numpy as np
from itertools import groupby


def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)


def print_tsv(filename, names):
    with H5File(filename) as f:
        dsets = [np.array(f.get(name)) for name in names]

        assert all_equal([dset.shape[0] for dset in dsets]), \
            "Datasets must have identical number of rows"
        assert all([len(dset.shape) < 3] for dset in dsets), \
            "Datasets must be 1 or 2 dimensional"

        # dsets = [dset.reshape((-1, 1)) if len(dset.shape) < 2 else dset for dset in dsets]

        for t in zip(*dsets):
            print(*t)


if __name__ == '__main__':
    print_tsv(sys.argv[1], sys.argv[2:])
