#!/usr/bin/env python3
from h5py import File, Group, Dataset
import sys

def h5copy(src, tgt):
    for k, v in src.attrs.items():
        tgt.attrs[k] = v

    for k, v in src.items():
        if isinstance(v, Group):
            h5copy(v, tgt.create_group(k))
        elif isinstance(v, Dataset):
            if v.chunks == None:
                tgt.create_dataset(k, data=v)
            else:
                tgt.create_dataset(k, data=v, maxshape=v.maxshape, chunks=v.chunks)

        else:
            print("Ignoring", k)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"Usage: {sys.argv[0]} SOURCE_FILE TARGET_FILE")

    src_name = sys.argv[1]
    tgt_name = sys.argv[2]

    with File(src_name) as src,\
         File(tgt_name, "w", libver='v110') as tgt:
        tgt.swmr_mode = True
        h5copy(src, tgt)
