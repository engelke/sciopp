# ScIO++ - Scientific Input/Output for C++

The Hierarchical Data Format v5 is a flexible and well used data format. Unfortunately, the official C++ interface is extremely cumbersome. The following example shows, how a simple series of numbers is written to an expanding file.

First, using this  library
```c++
/* open a file, overwrite contents */
H5File file("/tmp/foo.h5", H5File::Overwrite);

/* create a new streamable dataset for a std::array<double, 3>
 * with a chunk size of 64  */
auto stream = file.create_stream<std::array<double, 3>>("positions", 64);

/* write the data */
for (double phi = 0; phi < 4 * M_PI; phi += 0.01)
{
    stream.push({cos(phi), sin(phi), phi / M_PI});
}
```
Then, using the official interface
```c++
/* open a file, overwrite contents */
H5::H5File("/tmp/foo.h5", H5F_ACC_TRUNC);

/* create a chunked, extendable dataset */
const size_t N = 3;
using Vector = std::array<double, N>;
hsize_t chunk_size = 64;
hsize_t dim[2] = {0, N};
hsize_t max_dim = {H5S_UNLIMITED, N};
H5::DataSpace dspace(2, dim, max_dim);

H5::DSetCreatPropList prop;
hsize_t chunk_dim[2] = {chunk_size, N};
prop.setChunk(2, chunk_dim);

auto dset = file.createDataSet("positions", PredType::NATIVE_DOUBLE, dspace, prop);

hsize_t offset[2] = {0, N};
hsize_t el_dim = {1, N};
H5::DataSpace mspace(2, el_dim, NULL);
for (double phi = 0; phi < 4 * M_PI; phi += 0.01)
{
    /* extend the dataset and select last row */
    dim[0] += 1;
    dset.extend(dim);

    auto fspace  = dset.getSpace();
    fspace.selectHyperslab(H5S_SELECT_SET, el_dim, offset);
    offset[0] += 1;

    /* finally write the data */
    Vector v{cos(phi), sin(phi), phi / M_PI};
    dset.write(v.data(), PredType::NATIVE_DOUBLE, mspace, fspace)
}
```

It's important to note that this library does **not** attempt to offer a feature-complete interface to HDF5. Many people still use text-based file formats, because they are easy to understand and easy to implement, at first. This library tries to lower the barrier of using a binary file format and, thereby, enabling better inter-operability with other programming languages.

## Dependecies and Usage

* C++17, compile with `-std=c++17` (gcc 7 or higher)
* {fmt} library (https://fmt.dev/latest/index.html)
  for string formatting
  * Debian/Ubuntu
    ```bash
    sudo apt install libfmt-dev
    ```
  * MacOS
    ```bash
    brew install fmt
    ```

* HDF5 C++
  * Debian/Ubuntu
    ```bash
    sudo apt install hdf5 libhdf5-dev
    ```
  * MacOS
    ```bash
    brew install hdf5
    ```

This is a header only library, but the dependecies are not, so you have to link `libhdf5` and `libfmt`.

To make the compiler find the header files, either add a `-I` flag
```bash
g++ -std=c++17 -I /path/to/sciopp/include -c my_program.cpp
```
or add the path to your `CXX_INCLUDE_PATH`

```bash
export CXX_INCLUDE_PATH=/path/to/sciopp/include:$CXX_INCLUDE_PATH
```
in your rc files.

You can of course copy the `./include/sciopp` directory to any of your system's include directories, e.g. `~/.local/include`, if you prefer a more permanent setup:
```bash
# install to ~/.local
make install

# install to custom location, e.g. /usr/share
make install PREFIX=/usr/share
```

When linking, add `-lhdf5 -lhdf5_cpp -lfmt`
```bash
g++ -lhdf5 -lhdf5_cpp -lfmt my_program.o -o my_program
```
#### Debian
On Debian the HDF5 library is available as a serial version and a parallel MPI version. You have to tell both the compiler and the linker what version you want to use.
```bash
g++ ... -I/usr/include/hdf5/serial -c foo.cpp
g++ ... -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5 -lhdf5_cpp -lfmt ... foo.o -o ...
```
If you want a more flexible solution, have a look at the [Makefile](Makefile).

## Handling the file structure
Simply speaking, HDF5 files are structured like file systems. Groups can hold datasets or other groups, and all groups and datasets have a common ancestor, root.

#### Creating or opening files
Opening or creating of files is handled by the constructor of `H5File`
```c++
auto file = H5File("/tmp/some_data.h5", H5File::Overwrite);
```
It requires two arguments: the filename and a flag that specifies, what happens if a file with the same name already exists. Possible values for the flag are:
* `H5File::ReadOnly` open existing file in read only mode
* `H5File::ReadWrite` open existing file in read/write mode
* `H5File::Overwrite` open existing file in read/write mode, but delete any content
* `H5File::Create` create new file, fail if it already exists

By default, HDF5 does not allow simultaneous reading and writing by different programs.
This can be enabled by opening the file in HDF5's SWMR (single writer, multiple reader) mode.
`H5File::SWMR` can be combined with all modes listed above.
```c++
auto file = H5File("/tmp/swmr.h5", H5File::Overwrite | H5File::SWMR);
```
If a file was created without `HFile::SWMR`, it cannot be opened in SWMR mode later on.

#### Creating or opening groups
Groups can be created at the file root as well as inside any group
```c++
auto grp = file.create_group("grp");
auto child_grp = grp.create_group("child_grp");
```
Existing groups can be opened
```c++
auto grp = file.open_group("grp");
```

#### Listing content
All names of groups and dataset inside root or a group can be listed
```c++
grp.groups();    // returns a std::vector<std::string>
grp.datasets();
```
The returned vector is sorted alphabetically.

You can check if an object exists with `grp.exists("name")`

## Reading and Writing data
The `H5File` and `H5Group` offer methods for creating and reading datasets. The simplest way of writing data is to use the `write_dataset` method, which creates a fixed size dataset with the same shape as the container and writes the entire container.
```c++
std::vector<double> vec;
// ...
// fill data with some values
// ...
grp.write_dataset("vec", vec);
```
For reading, an equally simple method is available
```c++
auto vec2 = grp.read_dataset<std::vector<double>>("vec");
```
`read_dataset` requires the returned container type as a template argument. The container must have the same rank (i.e. number of dimensions) as the dataset, or `IncompaticleRank` will be thrown. If the container can be resized (e.g. `std::vector`), then the returned object will have the same size as the dataset. If a constant size container is provided (e.g. `std::array`), the dataset must have the same shape (i.e. number of elements per dimension) or `IncompaticleShape` will be thrown.

If more complex I/O is required, such as a dynamical size datasets or writing to parts of a dataset, the `H5DataSet` object is the way to go.

#### Creating or opening datasets

New datasets can be created using the `create_dataset(name, shape[, max_shape, chunk_shape])` method, which requires the atomic type of the buffer (the type of an individual element) and the rank as template arguments. The first argument of the function is the dataset name, the following specify shape and potential chunk shape. If only the `shape` is given, a fixed sized dataset is created
```c++
auto dset = grp.create_dataset<double, 2>("matrix", {8, 8});
```
If additionally `max_shape` and `chunk_shape` are given, a chunked dataset is created. Unlimited dimension can be achieved with `Unlimited`.
```c++
auto chunked = grp.create_dataset<double, 2>("table", {8, 8}, {Unlimited, 8}, {64, 8});
```
creates a chunked dataset, which can grow along the first axis. The data is stored in chunks of 64x8 elements within the file.

Existing dataset can be opened using `open_dataset`, which takes the atomic type and the rank as template arguments.
```c++
auto dset2 = grp.open_dataset<double, 2>("table")
```
`open_dataset` will check if the rank of the dataset equals the template argument.
If you want to open a dataset or create one if it does not exist, use `open_or_create_dataset`.
```c++
auto dset3 = grp.open_or_create_dataset<double, 2>("matrix", {8, 8});
```

#### Reading and writing to datasets

```c++
// return a new object containing the data
auto matrix = dset.read_as<Eigen::MatrixXd>();

// read the dataset into an existing container
Eigen::VectorXd v;
dset.read_to(v);
```

Data is written to the dataset with the `write` method. It expects the object that should be written as the first argument and a variable number of optional arguments:
* `Offset<Rank>` start writing at this element of the dataset. Defaults to `{0, 0, ...}`
* `Stride<Rank>` jump elements per dimension, default `{1, 1, ...}`
* `Reshape<Rank>` number of elements to write per dimension. Defaults to object shape if it has the same rank as the dataset. Must be provided if ranks differ.

The arguments can be provided in an arbitrary order. The rank template argument of the optional arguments must equal the dataset's rank.

```c++
auto dset = grp.create_dataset<double, 2>("matrix2", {16, 16});
Eigen::MatrixXd mat(8, 8);
dset.write(mat);                     // write starting at first row and col in dset
dset.write(mat, Offset<2>({5, 2}));  // write starting at 5th row and 2nd col in dset
dset.write(mat, Stride<2>({2, 2}));  // write every other row and column

/* writing buffers with different rank from the dataset requires the Reshape argument */
Eigen::VectorXd vec(4); // has rank 1
/* write {4, 0} through { 4, 3 } with the contents of vec */
dset.write(vec, Reshape<2>({1, 4}), Offset<2>({4, 0}));
```

## Writing streams
Streams are special buffered 1D dynamic size datasets. They are useful when dealing with timeseries, e.g. observables during a simulation. Streams have an internal buffer, that can hold a specified number of elements, in order to reduce the number of write operations. This is useful, when the individual objects are small (<1KB).
```c++
auto stream = grp.create_stream<Observables>("observables", 32);

for (int i = 0; i < niter; ++i)
{
    Observables obs;
    // ...
    // fill obs with data
    // ...
    stream.push(obs); // automatically writes every 32 push operations
}
// stream auto flushes, when going out of scope
```

Streams can also be opened.
```c++
auto stream1 = grp.open_stream<Observables>("observables", 32);
auto stream2 = grp.open_or_create_stream<Observables>("observables", 32);
```

## Writing tables
Sometimes data is stored as a set of 1d vectors with $N$ elements each, e.g. particle ids, positions and velocities, and the desired output is a 1d table, where the columns have names and different types. In this case, use the method `write_table`.
```c++
size_t n = 256;
Eigen::VectorXi ids(n);
Eigen::VectorXd xs(n), ys(n);
// ...
grp.write_table(Col("id", ids), Col("x", xs), Col("y", ys));
```
**Important:** This method will create a temporary object containing all the data in order to minimise the number of write operations. However, it is usually faster to write an individual dataset for each column.

## Supported containers
* `std::vector`
* `std::array`
* `Eigen::Matrix`, `Eigen::Array`, `Eigen::Vector`, both static and dynamic size.
   Keep in mind that `Eigen::Vector` is a just an `Eigen::Matrix` with shape $(N, 1)$. This library will interpret all `Eigen::Matrix` or `Eigen::Array` containers, where the number of columns or rows is set to a static size of $1$, as buffers of rank 1, i.e. vectors. Dynamic size containers, such as `Eigen::MatrixXd`, that have a current number of columns or rows of 1 are treated as buffers of rank 2.

## Supported types
The `H5Type` method can defer the internal HDF5 datatype. All native types are supported:
* `short`
* `int`
* `long`
* `long long`
* `unsigned short`
* `unsigned int`
* `unsigned long`
* `unsigned long long`
* `float`
* `double`
* `long double`
* `char`

Additionally, static size containers can be deferred to HDF5 array types and can be used as atomic datatypes. For example
```c++
using Point = std::array<double, 3>;
std::vector<Points> points;
// ...
grp.write_dataset("points", points);
```
will create a 1d dataset, where each element is an array of three `double` types. This only works if the number of elements is known at compile time. A `std::vector` for `Point` would not work, because `std::vector` is not a fixed size container.

Supported fixed size containers are
* `std::array`
* `Eigen::Matrix` and `Eigen::Array` with fixed shape, e.g. `Eigen::Matrix3d` or `Eigen::Vector2i`.

Supported fixed size heterogeneous datatypes
* `std::tuple` is deferred to a compound datatype, where the name is the position in tuple.

## Registering custom types
For `H5DataSet` and `H5Stream` to work with custom structs and classes, compound datatypes need to be created. This task can be automated using the macro `SCIOPP_REGISTER_STRUCT` which requires at least 2 but supports up to 364 arguments. The first argument is the name of the struct (including the namespace if the macro is called from a different namespace as the defintion of the struct). The following arguments specify which members become part of the HDF5 compound type. If any members are structs, they need to be registered as well.
```c++
struct Observables
{
    int step;
    Eigen::Vector3d r, p;
    double E;
};
SCIOPP_REGISTER_STRUCT(Observables, step, r, p, E);
```

## Attributes
The HDF5 format allows attributes to be added to the file, groups and datasets. Attributes are key value pairs, where the key is always a string and the value can be anything that can fit in a dataset (scalar or multi-dimensional data). Attributes can be written and modified using the `set_attribute` method of `H5File`, `H5Group`, `H5DataSet` as well as `H5Stream`.
```c++
dset.set_attribute("key", value);
```
Attributes can also be read using the `get_attribute` method. This method requires the target datatype to be specified as a template argument
```c++
auto x = dset.get_attribute<double>("key");
```
If the attribute has the correct rank and shape or is resizable, `get_attribute` returns an object, otherwise `IncompaticleRank` or `IncompaticleShape` is thrown.

#### String attributes
Unfortunately, **only** when setting string attributes, the template parameter must be explicitly specified or the string must specified as `std::string` using literals
```c++
dset.set_attribute("foo", "A string value"); // Error: No matching function to call ...

using namespace std::string_literals;
dset.set_attribute("foo", "A string value"s); // Preferred way

dset.set_attribute<std::string>("foo", "A string value"); // OK
std::string str = "Another string";
dset.set_attribute("bar", str); // OK
```
because `"abc"` strings are of type `char*` and template specialization is not possible for pointer type values.

## Examples
In addition to this README, you may find a few example programs in the [examples directory](examples). These can be compiled with
```bash
make examples
```
