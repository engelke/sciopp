#ifndef SCIOPP_CORE_BUFFER_HPP
#define SCIOPP_CORE_BUFFER_HPP
#include "sciopp/core/data.hpp"
#include "sciopp/core/shape.hpp"
#include "sciopp/core/types.hpp"

namespace sciopp
{
    template <class ContainerType,
              class ShapeHelperType = ShapeHelper<ContainerType>,
              class TypeHelperType =
                  H5TypeHelper<typename ContainerTraits<ContainerType>::Atomic>,
              class DataAccessorType = DataAccessor<ContainerType>>
    class H5Buffer
    {
      public:
        using traits = ContainerTraits<ContainerType>;
        using Container = ContainerType; // typename traits::Container;
        using Atomic = typename traits::Atomic;
        using Shape = typename traits::Shape;
        static const size_t rank = traits::rank;
        static const bool is_resizable = traits::is_resizable;

        H5Buffer(ContainerType& buffer)
            : m_buffer(buffer), m_dtype(TypeHelperType().type())
        {}

        const Container& get() const { return m_buffer; }
        Container& get() { return m_buffer; }


        H5::DataSpace dspace() const
        {
            if constexpr (rank == 0)
            {
                return H5::DataSpace();
            }
            else
            {
                /* update shape member before creating dataspace */
                auto s = shape();
                return H5::DataSpace(rank, s.data());
            }
        }

        const H5::DataType& dtype() const { return m_dtype; }
        Shape shape() const { return ShapeHelperType().shape(m_buffer); }
        size_t size() const { return ShapeHelperType().size(m_buffer); }

        template <bool Enable = is_resizable>
        std::enable_if_t<Enable, void> reshape(const Shape& new_shape)
        {
            ShapeHelperType().reshape(m_buffer, new_shape);
        }

        Atomic* data() { return DataAccessorType().data(m_buffer); }
        const Atomic* data() const { return DataAccessorType().data(m_buffer); }

        bool is_same_shape(const Shape& other_shape)
        {
            auto this_shape = shape();
            for (size_t i = 0; i < rank; ++i)
            {
                if (other_shape[i] != this_shape[i]) return false;
            }
            return true;
        }

      private:
        Container& m_buffer;
        H5::DataType m_dtype;
    };

    template <class ContainerType,
              class ShapeHelperType = ShapeHelper<ContainerType>,
              class TypeHelperType =
                  H5TypeHelper<typename ContainerTraits<ContainerType>::Atomic>,
              class DataAccessorType = DataAccessor<ContainerType>>
    class H5ConstBuffer
    {
      public:
        using traits = ContainerTraits<ContainerType>;
        using Container = typename traits::Container;
        using Atomic = typename traits::Atomic;
        using Shape = typename traits::Shape;
        static const size_t rank = traits::rank;
        static const bool is_resizable = traits::is_resizable;

        H5ConstBuffer(const ContainerType& buffer)
            : m_buffer(buffer), m_dtype(TypeHelperType().type())
        {}

        const Container& get() const { return m_buffer; }


        H5::DataSpace dspace() const
        {
            if constexpr (rank == 0)
            {
                return H5::DataSpace();
            }
            else
            {
                /* update shape member before creating dataspace */
                auto s = shape();
                return H5::DataSpace(rank, s.data());
            }
        }

        const H5::DataType& dtype() const { return m_dtype; }
        Shape shape() const { return ShapeHelperType().shape(m_buffer); }
        size_t size() const { return ShapeHelperType().size(m_buffer); }
        const Atomic* data() const { return DataAccessorType().data(m_buffer); }

        bool is_same_shape(const Shape& other_shape)
        {
            auto this_shape = shape();
            for (size_t i = 0; i < rank; ++i)
            {
                if (other_shape[i] != this_shape[i]) return false;
            }
            return true;
        }

      private:
        const Container& m_buffer;
        H5::DataType m_dtype;
    };


} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_BUFFER_HPP */
