#ifndef SCIOPP_TYPE_HELPERS_COMPOUND_HPP
#define SCIOPP_TYPE_HELPERS_COMPOUND_HPP

#define SCIOPP_COMP_INSERT(COMP, OBJECT, MEMBER)                                         \
    COMP.insertMember(#MEMBER, HOFFSET(OBJECT, MEMBER),                                  \
                      ::sciopp::H5Type<decltype(OBJECT::MEMBER)>());


/* Copied and modified from https://github.com/swansontec/map-macro/blob/master/map.h */
#define SCIOPP_EVAL0(...) __VA_ARGS__
#define SCIOPP_EVAL1(...) SCIOPP_EVAL0(SCIOPP_EVAL0(SCIOPP_EVAL0(__VA_ARGS__)))
#define SCIOPP_EVAL2(...) SCIOPP_EVAL1(SCIOPP_EVAL1(SCIOPP_EVAL1(__VA_ARGS__)))
#define SCIOPP_EVAL3(...) SCIOPP_EVAL2(SCIOPP_EVAL2(SCIOPP_EVAL2(__VA_ARGS__)))
#define SCIOPP_EVAL4(...) SCIOPP_EVAL3(SCIOPP_EVAL3(SCIOPP_EVAL3(__VA_ARGS__)))
#define SCIOPP_EVAL(...) SCIOPP_EVAL4(SCIOPP_EVAL4(SCIOPP_EVAL4(__VA_ARGS__)))

#define SCIOPP_MAP_END(...)
#define SCIOPP_MAP_OUT

#define SCIOPP_GET_END2() 0, SCIOPP_MAP_END
#define SCIOPP_GET_END1(...) SCIOPP_GET_END2
#define SCIOPP_GET_END(...) SCIOPP_GET_END1
#define SCIOPP_NEXT0(test, next, ...) next SCIOPP_MAP_OUT
#define SCIOPP_NEXT1(test, next) SCIOPP_NEXT0(test, next, 0)
#define SCIOPP_NEXT(test, next) SCIOPP_NEXT1(SCIOPP_GET_END test, next)

/* Add a and b as permanent arguments */
#define SCIOPP_MAP0(a, b, x, peek, ...)                                                  \
    SCIOPP_COMP_INSERT(a, b, x) SCIOPP_NEXT(peek, SCIOPP_MAP1)(a, b, peek, __VA_ARGS__)
#define SCIOPP_MAP1(a, b, x, peek, ...)                                                  \
    SCIOPP_COMP_INSERT(a, b, x) SCIOPP_NEXT(peek, SCIOPP_MAP0)(a, b, peek, __VA_ARGS__)

#define SCIOPP_INSERT_MANY(a, b, ...)                                                    \
    SCIOPP_EVAL(SCIOPP_MAP1(a, b, __VA_ARGS__, ()()(), ()()(), ()()(), 0))
/* End https://github.com/swansontec/map-macro/blob/master/map.h */

#define SCIOPP_REGISTER_STRUCT(obj, ...)                                                 \
    template <>                                                                          \
    struct sciopp::H5TypeHelper<obj>                                                     \
    {                                                                                    \
        H5::CompType type()                                                              \
        {                                                                                \
            H5::CompType t(sizeof(obj));                                                 \
            SCIOPP_INSERT_MANY(t, obj, __VA_ARGS__);                                     \
            return t;                                                                    \
        }                                                                                \
    };

#endif /* end of include guard: SCIOPP_TYPE_HELPERS_COMPOUND_HPP */
