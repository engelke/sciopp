#ifndef SCIOPP_CORE_SHAPE_HPP
#define SCIOPP_CORE_SHAPE_HPP
#include "sciopp/core/traits.hpp"

namespace sciopp
{
    template <class Container>
    struct ShapeHelper;

    template <class Container>
    ShapeHelper<Container> make_shape_helper(const Container& c)
    {
        return ShapeHelper<Container>();
    }

    template <class Atomic>
    struct ShapeHelper<std::vector<Atomic>>
    {
        using traits = ContainerTraits<std::vector<Atomic>>;
        using Shape = typename traits::Shape;
        using Container = typename traits::Container;
        Shape shape(const Container& v) { return Shape{v.size()}; }
        size_t size(const Container& v) { return v.size(); }
        void reshape(Container& v, const Shape& new_shape) { v.resize(new_shape[0]); }
    };

    template <class Atomic, size_t N>
    struct ShapeHelper<std::array<Atomic, N>>
    {
        using traits = ContainerTraits<std::array<Atomic, N>>;
        using Shape = typename traits::Shape;
        using Container = typename traits::Container;
        Shape shape(const Container& v) { return Shape{v.size()}; }
        size_t size(const Container& v) { return v.size(); }
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_SHAPE_HPP */
