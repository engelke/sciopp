#ifndef SCIOPP_CORE_DECLARATIONS_HPP
#define SCIOPP_CORE_DECLARATIONS_HPP
#include <array>

namespace sciopp
{
    template <class Container>
    struct ContainerTraits;
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_DECLARATIONS_HPP */
