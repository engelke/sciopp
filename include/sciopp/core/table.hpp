#ifndef SCIOPP_CORE_TABLE_HPP
#define SCIOPP_CORE_TABLE_HPP
#include "buffer.hpp"
#include <tuple>

namespace sciopp
{
    template <class T>
    using GetAtomic = typename T::Atomic;

    template <class Container>
    struct Col
    {
        using Atomic = typename ContainerTraits<Container>::Atomic;
        using BufferType = H5ConstBuffer<Container>;

        static_assert(BufferType::rank == 1, "Columns must have rank 1");

        Col(const std::string& p_name, const Container& c) : name(p_name), buffer(c) {}
        std::string name;
        BufferType buffer;
    };

    template <class... ColumnTypes>
    class Table
    {
      public:
        static const size_t rank = 1;

        using Atomic = std::tuple<ColumnTypes...>;
        Table() : m_dtype(sizeof(Atomic)) {}

        template <size_t Index, class... Cols>
        void insert_columns(Cols&&... cols)
        {
            // TODO static check same type of col and Atomic
            auto col = std::get<Index>(std::forward_as_tuple(cols...));
            size_t offset = reinterpret_cast<char*>(&std::get<Index>(m_dummy))
                          - reinterpret_cast<char*>(&m_dummy);


            /* insert column type into compound type */
            m_dtype.insertMember(
                col.name, offset,
                H5Type<typename std::tuple_element<Index, Atomic>::type>());

            /* resize internal buffer at first column */
            if constexpr (Index == 0) m_buffer.resize(col.buffer.size());

            /* copy buffer */
            for (size_t i = 0; i < m_buffer.size(); ++i)
            {
                std::get<Index>(m_buffer[i]) = *(col.buffer.data() + i);
            }

            if constexpr (Index < sizeof...(Cols) - 1)
                insert_columns<Index + 1, Cols...>(std::forward<Cols>(cols)...);
        }

        const auto& get() const { return m_buffer; }
        const auto* data() const { return m_buffer.data(); }
        const auto& dtype() const { return m_dtype; }
        auto shape() const { return ShapeArray<1>{m_buffer.size()}; }
        auto size() const { return m_buffer.size(); }
        auto dspace() const
        {
            hsize_t s = m_buffer.size();
            return H5::DataSpace(1, &s);
        }

      private:
        Atomic m_dummy;
        H5::CompType m_dtype;
        std::vector<Atomic> m_buffer;
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_TABLE_HPP */
