#ifndef SCIOPP_CORE_TRAITS_HPP
#define SCIOPP_CORE_TRAITS_HPP
#include "sciopp/core/declarations.hpp"
#include <H5Cpp.h>
#include <array>
#include <vector>

namespace sciopp
{
    template <size_t Rank>
    using ShapeArray = std::array<hsize_t, Rank>;

    template <class AtomicType>
    struct ContainerTraits<std::vector<AtomicType>>
    {
        using Atomic = AtomicType;
        using Container = std::vector<Atomic>;
        static const bool is_resizable = true;
        static const size_t rank = 1;
        using Shape = std::array<hsize_t, rank>;
    };

    template <class AtomicType, size_t N>
    struct ContainerTraits<std::array<AtomicType, N>>
    {
        using Atomic = AtomicType;
        using Container = std::array<Atomic, N>;
        static const bool is_resizable = false;
        static const size_t rank = 1;
        using Shape = std::array<hsize_t, rank>;
    };

#define SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(TYPE)                                  \
    template <>                                                                          \
    struct ContainerTraits<TYPE>                                                         \
    {                                                                                    \
        using Atomic = TYPE;                                                             \
        using Container = TYPE;                                                          \
        static const bool is_resizable = false;                                          \
        static const size_t rank = 0;                                                    \
        using Shape = std::array<hsize_t, rank>;                                         \
    };

    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(short)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(int)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(long)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(long long)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(unsigned short)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(unsigned int)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(unsigned long)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(unsigned long long)

    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(float)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(double)
    SCIOPP_SPECIALIZE_CONTAINER_TRAITS_NATIVE(long double)

} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_TRAITS_HPP */
