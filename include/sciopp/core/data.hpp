#ifndef SCIOPP_CORE_DATA_HPP
#define SCIOPP_CORE_DATA_HPP
#include "sciopp/core/traits.hpp"
#include <array>
#include <vector>

namespace sciopp
{
    template <class Container>
    struct DataAccessor
    {
        using traits = ContainerTraits<Container>;
        using Atomic = typename traits::Atomic;
        Atomic* data(Container& c) const { return c.data(); }
        const Atomic* data(const Container& c) const { return c.data(); }
    };

#define SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(TYPE)                                     \
    template <>                                                                          \
    struct DataAccessor<TYPE>                                                            \
    {                                                                                    \
        using traits = ContainerTraits<TYPE>;                                            \
        using Atomic = typename traits::Atomic;                                          \
        Atomic* data(TYPE& c) const { return &c; }                                       \
        const Atomic* data(const TYPE& c) const { return &c; }                           \
    };

    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(short)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(int)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(long)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(long long)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(unsigned short)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(unsigned int)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(unsigned long)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(unsigned long long)

    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(float)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(double)
    SCIOPP_SPECIALIZE_DATA_ACCESSOR_NATIVE(long double)
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_DATA_HPP */
