#ifndef SCIOPP_CORE_TYPES_HPP
#define SCIOPP_CORE_TYPES_HPP
#include <H5Cpp.h>
#include <array>
#include <fmt/core.h>
#include <string>
#include <tuple>

namespace sciopp
{
    template <class T>
    struct H5TypeHelper;

    template <class T, int... Ns>
    struct H5StaticArrayHelper
    {
        static constexpr hsize_t rank = sizeof...(Ns);
        static constexpr std::array<hsize_t, rank> shape{Ns...};

        H5::DataType type()
        {
            return H5::ArrayType(H5TypeHelper<T>().type(), rank, shape.data());
        }
    };

    template <class T>
    struct H5Type : H5::DataType
    {
        H5Type(const T&) : H5::DataType(H5TypeHelper<T>().type()) {}
        H5Type() : H5::DataType(H5TypeHelper<T>().type()) {}
    };

#define SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(TYPE, H5TYPE)                               \
    template <>                                                                          \
    struct H5TypeHelper<TYPE>                                                            \
    {                                                                                    \
        H5::DataType type() { return H5::PredType::H5TYPE; }                             \
    };

    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(char, NATIVE_CHAR)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(short, NATIVE_SHORT)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(int, NATIVE_INT)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(long, NATIVE_LONG)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(long long, NATIVE_LLONG)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(unsigned short, NATIVE_USHORT)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(unsigned int, NATIVE_UINT)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(unsigned long, NATIVE_ULONG)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(unsigned long long, NATIVE_ULLONG)

    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(float, NATIVE_FLOAT)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(double, NATIVE_DOUBLE)
    SCIOPP_SPECIALIZE_TYPE_HELPER_NATIVE(long double, NATIVE_LDOUBLE)

    template <>
    struct H5TypeHelper<std::string>
    {
        H5::DataType type() { return H5::StrType(H5::PredType::C_S1, H5T_VARIABLE); }
    };

    template <class T, size_t N>
    struct H5TypeHelper<std::array<T, N>>
    {
        H5::DataType type() { return H5StaticArrayHelper<T, N>().type(); }
    };

    template <class... Types>
    struct H5TypeHelper<std::tuple<Types...>>
    {
        H5TypeHelper() : m_dtype(sizeof(Tuple)) { insert_member(); }
        const H5::DataType& type() { return m_dtype; }

      private:
        using Tuple = std::tuple<Types...>;
        const Tuple m_dummy;
        H5::CompType m_dtype;

        template <size_t N = 0>
        void insert_member()
        {
            /* calculate offset */
            size_t offset = reinterpret_cast<const char*>(&std::get<N>(m_dummy))
                          - reinterpret_cast<const char*>(&m_dummy);

            /* insert member, use position as name */
            using Type = typename std::tuple_element<N, Tuple>::type;
            m_dtype.insertMember(fmt::format("{}", N), offset, H5Type<Type>());

            if constexpr (N < std::tuple_size<Tuple>::value - 1) insert_member<N + 1>();
        }
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_TYPES_HPP */
