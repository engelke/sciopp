#ifndef SCIOPP_EXCEPTIONS_HPP
#define SCIOPP_EXCEPTIONS_HPP
#include <exception>

namespace sciopp
{
    struct NotImplemented : public std::exception
    {
        virtual const char* what() const noexcept { return "Has not been implemented."; }
    };

    struct UnknownFlag : public std::exception
    {
        virtual const char* what() const noexcept { return "Unknown flag."; }
    };

    struct NotResizable : public std::exception
    {
        virtual const char* what() const noexcept { return "Buffer cannot be resized."; }
    };

    struct IncompatibleRank : public std::exception
    {
        virtual const char* what() const noexcept
        {
            return "Rank of dataset in file differs from H5DataSet::rank";
        }
    };

    struct IncompatibleShape : public std::exception
    {
        virtual const char* what() const noexcept
        {
            return "Shape of dataset differs from shape of buffer and cannot be resized";
        }
    };

    struct IncompatibleSize : public std::exception
    {
        virtual const char* what() const noexcept
        {
            return "Size of selected elements in dataset differs from size of buffer";
        }
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_EXCEPTIONS_HPP */
