#ifndef SCIOPP_HDF5_GROUP_HPP
#define SCIOPP_HDF5_GROUP_HPP
#include "sciopp/mixins/attribute.hpp"
#include "sciopp/mixins/node.hpp"

namespace sciopp
{
    class H5Group : public NodeMixin<H5Group>, public AttributeMixin<H5Group>
    {
      public:
        H5Group() = default;
        H5Group(const H5::Group& p_group) : m_group(p_group) {}

        static inline H5Group create(H5::H5Location& loc, const std::string& name)
        {
            return H5Group(loc.createGroup(name));
        }

        static inline H5Group open(H5::H5Location& loc, const std::string& name)
        {
            return H5Group(loc.openGroup(name));
        }

        H5::Group& h5() { return m_group; }
        const H5::Group& h5() const { return m_group; }

      private:
        H5::Group m_group;
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_HDF5_GROUP_HPP */
