#ifndef SCIOPP_HDF5_DECLARATIONS_HPP
#define SCIOPP_HDF5_DECLARATIONS_HPP
#include "sciopp/core/declarations.hpp"
#include <H5Cpp.h>
#include <cstddef>
#include <limits>

namespace sciopp
{
    class H5File;
    class H5Group;

    template <class Atomic, size_t Rank>
    class H5DataSet;

    template <class Container>
    using GenericDataSet = H5DataSet<typename ContainerTraits<Container>::Atomic,
                                     ContainerTraits<Container>::rank>;

    template <class Atomic>
    class H5Stream;

    template <class T>
    class Attribute;

    const hsize_t Unlimited = std::numeric_limits<hsize_t>::max();
} // namespace sciopp

#endif /* end of include guard: SCIOPP_HDF5_DECLARATIONS_HPP */
