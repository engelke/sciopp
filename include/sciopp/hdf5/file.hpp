#ifndef SCIOPP_HDF5_FILE_HPP
#define SCIOPP_HDF5_FILE_HPP
#include "sciopp/mixins/attribute.hpp"
#include "sciopp/mixins/node.hpp"

namespace sciopp
{
    class H5File : public NodeMixin<H5File>, public AttributeMixin<H5File>
    {
      public:
        /* Check hdf5/serial/H5Fpublic.h:50 for definitions */
        enum : unsigned
        {
            /* Open in read only mode */
            ReadOnly = 1 << 0,
            /* Open in read/write mode */
            ReadWrite = 1 << 1,
            /* Open in read/write mode and delete content or create */
            Overwrite = 1 << 2,
            /* Create new */
            Create = 1 << 3,
            /* Enable SWMR */
            SWMR = 1 << 4,
        };

        H5File() = default;
        inline H5File(const std::string& name, unsigned flags)
            : m_file(name, get_h5flags(flags))
        {}

        inline void open(const std::string& name, unsigned flags)
        {
            m_file.openFile(name, get_h5flags(flags));
        }
        inline void close() { m_file.close(); }

        inline void flush() { m_file.flush(H5F_SCOPE_GLOBAL); }


        H5::H5File& h5() { return m_file; }
        const H5::H5File& h5() const { return m_file; }

      private:
        H5::H5File m_file;

        static inline unsigned get_h5flags(unsigned flags)
        {
            unsigned result;
            if (flags & ReadOnly)
                result = H5F_ACC_RDONLY;
            else if (flags & ReadWrite)
                result = H5F_ACC_RDWR;
            else if (flags & Overwrite)
                result = H5F_ACC_TRUNC;
            else if (flags & Create)
                result = H5F_ACC_EXCL;
            else
                throw UnknownFlag();

            if (flags & SWMR)
            {
                if (flags & ReadOnly)
                    result |= H5F_ACC_SWMR_READ;
                else
                    result |= H5F_ACC_SWMR_WRITE;
            }

            return result;
        }
    }; // namespace sciopp
} // namespace sciopp

#endif /* end of include guard: SCIOPP_HDF5_FILE_HPP */
