#ifndef SCIOPP_HDF5_STREAM_HPP
#define SCIOPP_HDF5_STREAM_HPP
#include "sciopp/hdf5/dataset.hpp"

namespace sciopp
{
    template <class Atomic>
    class H5Stream : public H5DataSet<Atomic, 1>
    {
      public:
        using Base = H5DataSet<Atomic, 1>;
        H5Stream(const Base& p_dset, size_t p_buffer_size)
            : Base(p_dset), m_buffer_size(p_buffer_size)
        {}

        ~H5Stream() { flush(); }

        static H5Stream create(H5::H5Location& p_loc,
                               const std::string& p_name,
                               size_t p_buffer_size,
                               size_t p_chunk_size = (4096 / sizeof(Atomic)),
                               const H5::DataType& p_dtype = H5Type<Atomic>());

        static H5Stream
        open(H5::H5Location& p_loc, const std::string& p_name, size_t p_buffer_size);

        void push(const Atomic& o);

        template <class Container>
        void append(const Container& c);

        void flush();


      private:
        hsize_t m_buffer_size;
        std::vector<Atomic> m_buffer;
    };

    template <class Atomic>
    H5Stream<Atomic> H5Stream<Atomic>::create(H5::H5Location& p_loc,
                                              const std::string& p_name,
                                              size_t p_buffer_size,
                                              size_t p_chunk_size,
                                              const H5::DataType& p_dtype)
    {
        return H5Stream<Atomic>(H5DataSet<Atomic, 1>::create(p_loc, p_name, {0},
                                                             {H5S_UNLIMITED},
                                                             {p_chunk_size}, p_dtype),
                                p_buffer_size);
    }

    template <class Atomic>
    H5Stream<Atomic> H5Stream<Atomic>::open(H5::H5Location& p_loc,
                                            const std::string& p_name,
                                            size_t p_buffer_size)
    {
        return H5Stream<Atomic>(H5DataSet<Atomic, 1>::open(p_loc, p_name), p_buffer_size);
    }

    template <class Atomic>
    void H5Stream<Atomic>::push(const Atomic& o)
    {
        if (m_buffer_size != 1)
        {
            m_buffer.push_back(o);

            if (m_buffer.size() == m_buffer_size)
            {
                append(m_buffer);
                m_buffer.clear();
            }
        }
        else
        {
            auto offset = Base::shape();
            Base::extend({1});
            auto fspace = Base::h5().getSpace();
            fspace.selectHyperslab(H5S_SELECT_SET, &m_buffer_size, offset.data());
            auto bspace = H5::DataSpace(1, &m_buffer_size, nullptr);
            Base::h5().write(&o, H5Type<Atomic>(), bspace, fspace);
        }
    }

    template <class Atomic>
    void H5Stream<Atomic>::flush()
    {
        if (m_buffer.size() > 0)
        {
            append(m_buffer);
            m_buffer.clear();
        }
    }


    template <class Atomic>
    template <class Container>
    void H5Stream<Atomic>::append(const Container& c)
    {
        auto buffer = H5ConstBuffer(c);
        auto offset = Base::shape();
        Base::extend(buffer.shape());
        Base::write_buffer(buffer, Offset<1>(offset));
    }
} // namespace sciopp

#endif /* end of include guard: SCIOPP_HDF5_STREAM_HPP */
