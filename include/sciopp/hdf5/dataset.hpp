#ifndef SCIOPP_HDF5_DATASET_HPP
#define SCIOPP_HDF5_DATASET_HPP
#include "sciopp/core/buffer.hpp"
#include "sciopp/hdf5/exceptions.hpp"
#include "sciopp/misc/array_init.hpp"
#include "sciopp/misc/kwargs.hpp"
#include "sciopp/mixins/attribute.hpp"

namespace sciopp
{
    /* Define keyword argument types */
    template <size_t Rank>
    using Offset = KeywordArgument<ShapeArray<Rank>, struct OffsetTag>;

    template <size_t Rank>
    using Stride = KeywordArgument<ShapeArray<Rank>, struct StrideTag>;

    template <size_t Rank>
    using Reshape = KeywordArgument<ShapeArray<Rank>, struct ReshapeTag>;

    template <class AtomicType, size_t Rank>
    class H5DataSet : public AttributeMixin<H5DataSet<AtomicType, Rank>>
    {
      public:
        using Atomic = AtomicType;
        static const size_t rank = Rank;
        using Shape = std::array<hsize_t, rank>;

        H5DataSet() = default;
        H5DataSet(const H5::DataType& p_dtype,
                  const Shape& p_shape,
                  const Shape& p_max_shape = Shape(),
                  const Shape& p_chunk_shape = Shape())
            : m_dtype(p_dtype),
              m_shape(p_shape),
              m_max_shape(p_max_shape),
              m_chunk_shape(p_chunk_shape)
        {}

        static H5DataSet open(H5::H5Location& loc, const std::string& name);
        static H5DataSet create(H5::H5Location& p_loc,
                                const std::string& p_name,
                                const Shape& p_shape,
                                const H5::DataType& p_dtype = H5Type<Atomic>());
        static H5DataSet create(H5::H5Location& p_loc,
                                const std::string& p_name,
                                const Shape& p_shape,
                                const Shape& p_max_shape,
                                const Shape& p_chunk_shape,
                                const H5::DataType& p_dtype = H5Type<Atomic>());

        const Shape& shape() const { return m_shape; }
        const Shape& max_shape() const { return m_max_shape; }
        void extend(Shape grow_by);

        template <class Container>
        void read_buffer(H5Buffer<Container>& buffer);

        template <class Container>
        Container read_as();
        template <class Container>
        void read_to(Container&);

        template <class Buffer, class... Args>
        void write_buffer(const Buffer& buffer, Args&&... args);

        template <class Container, class... Args>
        void write(const Container& c, Args&&... args);

        bool is_same_shape(const Shape&);

        H5::DataSet& h5() { return m_dset; }

      private:
        H5::DataType m_dtype; //< dataset in file
        Shape m_shape;        //< current shape of the dataset
        Shape m_max_shape;    //< maximum shape of the dataset
        Shape m_chunk_shape;  //< shape of chunks if dset is chunked
        H5::DataSet m_dset;   //< underlying H5::DataSet
    };


    /**
     * Open an existing dataset
     * @param p_loc  File or group where the dataset is located
     * @param p_name Name of the dataset
     */
    template <class AtomicType, size_t Rank>
    H5DataSet<AtomicType, Rank>
    H5DataSet<AtomicType, Rank>::open(H5::H5Location& p_loc, const std::string& p_name)
    {
        H5DataSet<AtomicType, Rank> dset;
        /* open dataset */
        dset.m_dset = p_loc.openDataSet(p_name);
        auto dspace = dset.m_dset.getSpace();

        /* check matching rank */
        if (dset.rank != dspace.getSimpleExtentNdims()) throw IncompatibleRank();

        /* read shape and dtype */
        dspace.getSimpleExtentDims(dset.m_shape.data(), dset.m_max_shape.data());
        dset.m_dtype = dset.m_dset.getDataType();

        /* read chunk size */
        auto prop = dset.m_dset.getCreatePlist();
        if (prop.getLayout() == H5D_CHUNKED)
        {
            prop.getChunk(dset.rank, dset.m_chunk_shape.data());
        }

        return dset;
    }

    /**
     * Create a contiguous dataset (not chunked) dataset
     * @param p_loc   File or group where the dataset should be created
     * @param p_name  Name of dataset
     * @param p_shape Shape of the dataset (must be known at creation for non-chunked)
     * @param p_dtype H5::DataType descriptor (deducted from AtomicType if omitted)
     */
    template <class AtomicType, size_t Rank>
    H5DataSet<AtomicType, Rank>
    H5DataSet<AtomicType, Rank>::create(H5::H5Location& p_loc,
                                        const std::string& p_name,
                                        const Shape& p_shape,
                                        const H5::DataType& p_dtype)
    {
        H5DataSet<AtomicType, Rank> dset(p_dtype, p_shape);
        auto dspace = H5::DataSpace(rank, dset.m_shape.data());
        dset.m_dset = p_loc.createDataSet(p_name, dset.m_dtype, dspace);
        return dset;
    }

    /**
     * Create a chunked dataset
     * @param p_loc         File or group where the dataset will be created
     * @param p_name        Name of the dataset
     * @param p_shape       Initial shape of the dataset
     * @param p_max_shape   Maximum shape of the dataset
     * @param p_chunk_shape Shape of each chunk
     * @param p_dtype       H5::DataType descriptor (deducted from AtomicType if omitted)
     */
    template <class AtomicType, size_t Rank>
    H5DataSet<AtomicType, Rank>
    H5DataSet<AtomicType, Rank>::create(H5::H5Location& p_loc,
                                        const std::string& p_name,
                                        const Shape& p_shape,
                                        const Shape& p_max_shape,
                                        const Shape& p_chunk_shape,
                                        const H5::DataType& p_dtype)
    {
        H5DataSet<AtomicType, Rank> dset(p_dtype, p_shape, p_max_shape, p_chunk_shape);
        auto dspace = H5::DataSpace(rank, dset.m_shape.data(), dset.m_max_shape.data());

        H5::DSetCreatPropList prop;
        prop.setChunk(rank, dset.m_chunk_shape.data());

        dset.m_dset = p_loc.createDataSet(p_name, dset.m_dtype, dspace, prop);
        return dset;
    }

    /**
     * Increase the dataset shape
     * @param grow_by Number of additional places per dimension
     */
    template <class AtomicType, size_t Rank>
    void H5DataSet<AtomicType, Rank>::extend(Shape grow_by)
    {
        for (size_t i = 0; i < rank; ++i)
        {
            grow_by[i] += m_shape[i];
        }
        m_dset.extend(grow_by.data());
        m_shape = grow_by;
    }


    /**
     * Write a buffer to a part of the dataset starting at offset
     * @param buffer H5Buffer object
     * @param offset Offset from dataset start
     * @param stride Stride in dataset
     */
    template <class AtomicType, size_t Rank>
    template <class Buffer, class... Args>
    void H5DataSet<AtomicType, Rank>::write_buffer(const Buffer& buffer, Args&&... args)
    {
        /* default offset {0, 0, ...} */
        Shape offset =
            get_arg_default<Offset<rank>>(ArrayInit<hsize_t, rank>(0), args...);
        /* default stride {1, 1, ...} */
        Shape stride =
            get_arg_default<Stride<rank>>(ArrayInit<hsize_t, rank>(1), args...);

        Shape shape;
        /* require reshape argument if buffer and dataset have different rank, else
         * default to buffer.shape() */
        if constexpr (rank == Buffer::rank)
            shape = get_arg_default<Reshape<rank>>(buffer.shape(), args...);
        else
            shape = get_arg<Reshape<rank>>(args...);

        auto fspace = m_dset.getSpace();
        fspace.selectHyperslab(H5S_SELECT_SET, shape.data(), offset.data(),
                               stride.data());
        if (static_cast<hssize_t>(buffer.size()) != fspace.getSelectNpoints())
            throw IncompatibleSize();

        m_dset.write(buffer.data(), buffer.dtype(), buffer.dspace(), fspace);
    }

    /**
     * Write a container to a part of the dataset starting at offset. Datatypes will be
     * deducted automatically
     * @param c      Container to write
     * @param offset Offset from dataset start
     */
    template <class AtomicType, size_t Rank>
    template <class Container, class... Args>
    void H5DataSet<AtomicType, Rank>::write(const Container& c, Args&&... args)

    {
        const H5ConstBuffer b(c);
        write_buffer(b, args...);
    }
    /**
     * Read a complete dataset into a buffer, resizing the buffer if necessary
     * @param buffer [description]
     */
    template <class AtomicType, size_t Rank>
    template <class Container>
    void H5DataSet<AtomicType, Rank>::read_buffer(H5Buffer<Container>& buffer)
    {
        using Buffer = H5Buffer<Container>;
        static_assert(rank == Buffer::rank, "Dataset and Buffer must have same rank");
        if (!is_same_shape(buffer.shape()))
        {
            if constexpr (!Buffer::is_resizable)
                throw IncompatibleShape();
            else
                buffer.reshape(m_shape);
        }

        auto fspace = m_dset.getSpace();
        m_dset.read(buffer.data(), buffer.dtype(), buffer.dspace(), fspace);
    }

    /**
     * Read dataset and return an object of specified container format
     */
    template <class AtomicType, size_t Rank>
    template <class Container>
    Container H5DataSet<AtomicType, Rank>::read_as()
    {
        Container tmp;
        auto b = H5Buffer<Container>(tmp);
        read_buffer(b);
        return tmp;
    }

    /**
     * Read dataset and return an object of specified container format
     */
    template <class AtomicType, size_t Rank>
    template <class Container>
    void H5DataSet<AtomicType, Rank>::read_to(Container& c)
    {
        auto b = H5Buffer<Container>(c);
        read_buffer(b);
    }


    /**
     * Check if dataset has the same shape as other_shape
     * @param other_shape Shape to test
     */
    template <class AtomicType, size_t Rank>
    bool H5DataSet<AtomicType, Rank>::is_same_shape(const Shape& other_shape)
    {
        for (size_t i = 0; i < rank; ++i)
        {
            if (other_shape[i] != m_shape[i]) return false;
        }
        return true;
    }
} // namespace sciopp

#endif /* end of include guard: SCIOPP_HDF5_DATASET_HPP */
