#ifndef SCIOPP_CORE_HPP
#define SCIOPP_CORE_HPP

#include "sciopp/core/buffer.hpp"
#include "sciopp/core/compound.hpp"
#include "sciopp/hdf5/dataset.hpp"
#include "sciopp/hdf5/file.hpp"
#include "sciopp/hdf5/group.hpp"
#include "sciopp/hdf5/stream.hpp"

#include "sciopp/mixins/node_impl.hpp"

namespace sciopp
{
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_HPP */
