#ifndef SCIOPP_CORE_ARRAY_INIT_HPP
#define SCIOPP_CORE_ARRAY_INIT_HPP
#include <algorithm>
#include <array>

namespace sciopp
{
    template <class T, size_t N>
    struct ArrayInit
    {
        ArrayInit(const T& v) { std::fill(std::begin(value), std::end(value), v); }

        operator std::array<T, N>() const { return value; }

        std::array<T, N> value;
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_CORE_ARRAY_INIT_HPP */
