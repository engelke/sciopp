#ifndef SCIOPP_MISC_KWARGS_HPP
#define SCIOPP_MISC_KWARGS_HPP
#include <tuple>
#include <type_traits>

namespace sciopp
{
    template <class T, class Tag>
    struct KeywordArgument
    {
        using value_type = T;
        KeywordArgument(const T& p_value) : value(p_value) {}
        KeywordArgument(T&& p_value) : value(std::move(p_value)) {}
        operator T() { return value; }
        T value;
    };

    template <typename T, typename... Us>
    struct has_type : std::disjunction<std::is_same<T, Us>...>
    {
    };

    template <class Arg, class... Args>
    Arg get_arg(Args&&... args)
    {
        static_assert(has_type<Arg, std::remove_reference_t<Args>...>::value,
                      "Missing mandatory keyword argument");
        return std::get<Arg>(std::tuple(std::forward<Args>(args)...));
    }

    template <class Arg, class... Args>
    Arg get_arg_default(typename Arg::value_type&& def, Args&&... args)
    {
        if constexpr (has_type<Arg, std::remove_reference_t<Args>...>::value)
            return std::get<Arg>(std::tuple(std::forward<Args>(args)...));
        else
            return def;
    }

} // namespace sciopp

#endif /* end of include guard: SCIOPP_MISC_KWARGS_HPP */
