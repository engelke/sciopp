#ifndef SCIOPP_TYPE_HELPERS_EIGEN_HPP
#define SCIOPP_TYPE_HELPERS_EIGEN_HPP
#include "sciopp/core/types.hpp"
#include <Eigen/Dense>

namespace sciopp
{
    template <typename TT, int NRow, int NCol, int StorageOrder>
    struct H5TypeHelper<Eigen::Matrix<TT, NRow, NCol, StorageOrder>>;

#define SCIOPP_REGISTER_EIGEN_STATIC_1D(EIGENTYPE, NROW, NCOL, ST)                       \
    template <typename TT, int NEl>                                                      \
    struct H5TypeHelper<Eigen::EIGENTYPE<TT, NROW, NCOL, Eigen::ST>>                     \
    {                                                                                    \
        static_assert(NEl > 0, "Requiring finite dimension at compile time");            \
        H5::DataType type() { return H5StaticArrayHelper<TT, NEl>().type(); }            \
    };

    SCIOPP_REGISTER_EIGEN_STATIC_1D(Matrix, NEl, 1, RowMajor)
    SCIOPP_REGISTER_EIGEN_STATIC_1D(Matrix, NEl, 1, ColMajor)
    SCIOPP_REGISTER_EIGEN_STATIC_1D(Matrix, 1, NEl, RowMajor)
    SCIOPP_REGISTER_EIGEN_STATIC_1D(Matrix, 1, NEl, ColMajor)

    SCIOPP_REGISTER_EIGEN_STATIC_1D(Array, NEl, 1, RowMajor)
    SCIOPP_REGISTER_EIGEN_STATIC_1D(Array, NEl, 1, ColMajor)
    SCIOPP_REGISTER_EIGEN_STATIC_1D(Array, 1, NEl, RowMajor)
    SCIOPP_REGISTER_EIGEN_STATIC_1D(Array, 1, NEl, ColMajor)

#define SCIOPP_REGISTER_EIGEN_STATIC_2D(EIGENTYPE, ER, EC, ST, HR, HC)                   \
    template <typename TT, int NRow, int NCol>                                           \
    struct H5TypeHelper<Eigen::EIGENTYPE<TT, ER, EC, Eigen::ST>>                         \
    {                                                                                    \
        static_assert(NRow > 0 && NCol > 0, "Requiring finite dimension");               \
        H5::DataType type() { return H5StaticArrayHelper<TT, ER, EC>().type(); }         \
    };

    SCIOPP_REGISTER_EIGEN_STATIC_2D(Matrix, NRow, NCol, RowMajor, NRow, NCol)
    SCIOPP_REGISTER_EIGEN_STATIC_2D(Matrix, NRow, NCol, ColMajor, NCol, NRow)

    SCIOPP_REGISTER_EIGEN_STATIC_2D(Array, NRow, NCol, RowMajor, NRow, NCol)
    SCIOPP_REGISTER_EIGEN_STATIC_2D(Array, NRow, NCol, ColMajor, NCol, NRow)
} // namespace sciopp

#endif /* end of include guard: SCIOPP_TYPE_HELPERS_EIGEN_HPP */
