#ifndef SCIOPP_EIGEN_TRAITS_HPP
#define SCIOPP_EIGEN_TRAITS_HPP
#include "sciopp/core/traits.hpp"
#include <Eigen/Dense>
#include <type_traits>

namespace sciopp
{
    template <class AtomicType, int Row, int Col, int Sto>
    struct ContainerTraits<Eigen::Matrix<AtomicType, Row, Col, Sto>>
    {
        using Atomic = AtomicType;
        using Container = Eigen::Matrix<Atomic, Row, Col, Sto>;
        static constexpr bool is_resizable =
            Row == Eigen::Dynamic && Col == Eigen::Dynamic;
        static const size_t rank = 2;
        using Shape = std::array<hsize_t, rank>;
    };

    template <class AtomicType, int N, int Sto>
    struct ContainerTraits<Eigen::Matrix<AtomicType, N, 1, Sto>>
    {
        using Atomic = AtomicType;
        using Container = Eigen::Matrix<Atomic, N, 1>;
        static constexpr bool is_resizable = N == Eigen::Dynamic;
        static const size_t rank = 1;
        using Shape = std::array<hsize_t, rank>;
    };

    template <class AtomicType, int N, int Sto>
    struct ContainerTraits<Eigen::Matrix<AtomicType, 1, N, Sto>>
    {
        using Atomic = AtomicType;
        using Container = Eigen::Matrix<Atomic, 1, N>;
        static constexpr bool is_resizable = N == Eigen::Dynamic;
        static const size_t rank = 1;
        using Shape = std::array<hsize_t, rank>;
    };

    template <class AtomicType, int Row, int Col, int Sto>
    struct ContainerTraits<Eigen::Array<AtomicType, Row, Col, Sto>>
    {
        using Atomic = AtomicType;
        using Container = Eigen::Array<Atomic, Row, Col, Sto>;
        static constexpr bool is_resizable =
            Row == Eigen::Dynamic && Col == Eigen::Dynamic;
        static const size_t rank = 2;
        using Shape = std::array<hsize_t, rank>;
    };

    template <class AtomicType, int N, int Sto>
    struct ContainerTraits<Eigen::Array<AtomicType, N, 1, Sto>>
    {
        using Atomic = AtomicType;
        using Container = Eigen::Array<Atomic, N, 1, Sto>;
        static constexpr bool is_resizable = N == Eigen::Dynamic;
        static const size_t rank = 1;
        using Shape = std::array<hsize_t, rank>;
    };

    template <class AtomicType, int N, int Sto>
    struct ContainerTraits<Eigen::Array<AtomicType, 1, N, Sto>>
    {
        using Atomic = AtomicType;
        using Container = Eigen::Array<Atomic, 1, N, Sto>;
        static constexpr bool is_resizable = N == Eigen::Dynamic;
        static const size_t rank = 1;
        using Shape = std::array<hsize_t, rank>;
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_EIGEN_TRAITS_HPP */
