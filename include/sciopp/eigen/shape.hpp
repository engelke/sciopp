#ifndef SCIOP_EIGEN_SHAPE_HPP
#define SCIOP_EIGEN_SHAPE_HPP
#include "sciopp/core/shape.hpp"
#include "sciopp/eigen/traits.hpp"

namespace sciopp
{
    template <class Atomic, int NRow, int NCol, int St>
    struct ShapeHelper<Eigen::Matrix<Atomic, NRow, NCol, St>>
    {
        using traits = ContainerTraits<Eigen::Matrix<Atomic, NRow, NCol, St>>;
        using Shape = typename traits::Shape;
        using Container = Eigen::Matrix<Atomic, NRow, NCol, St>;

        Shape shape(const Container& v)
        {
            if constexpr (traits::rank == 2)
            {
                if constexpr (St == Eigen::RowMajor)
                    return Shape{static_cast<hsize_t>(v.rows()),
                                 static_cast<hsize_t>(v.cols())};
                else
                    return Shape{static_cast<hsize_t>(v.cols()),
                                 static_cast<hsize_t>(v.rows())};
            }
            else
            {
                return Shape{static_cast<hsize_t>(v.size())};
            }
        }
        size_t size(const Container& v) { return v.size(); }

        /* disabled if container is not resizable */
        template <class T = Container>
        std::enable_if_t<ContainerTraits<T>::is_resizable, void> reshape(Container& v,
                                                                         const Shape& s)
        {
            if constexpr (traits::rank == 2)
            {
                if constexpr (St == Eigen::RowMajor)
                    v.resize(s[0], s[1]);
                else
                    v.resize(s[1], s[0]);
            }
            else
            {
                v.resize(s[0]);
            }
        }
    };

    template <class Atomic, int NRow, int NCol, int St>
    struct ShapeHelper<Eigen::Array<Atomic, NRow, NCol, St>>
    {
        using traits = ContainerTraits<Eigen::Array<Atomic, NRow, NCol, St>>;
        using Shape = typename traits::Shape;
        using Container = typename traits::Container;

        Shape shape(const Container& v)
        {
            if constexpr (traits::rank == 2)
            {
                if constexpr (St == Eigen::RowMajor)
                    return Shape{static_cast<hsize_t>(v.rows()),
                                 static_cast<hsize_t>(v.cols())};
                else
                    return Shape{static_cast<hsize_t>(v.cols()),
                                 static_cast<hsize_t>(v.rows())};
            }
            else
            {
                return Shape{static_cast<hsize_t>(v.size())};
            }
        }
        size_t size(const Container& v) { return v.size(); }

        /* disabled if container is not resizable */
        template <class T = Container>
        std::enable_if_t<ContainerTraits<T>::is_resizable, void> reshape(Container& v,
                                                                         const Shape& s)
        {
            if constexpr (traits::rank == 2)
            {
                if constexpr (St == Eigen::RowMajor)
                    v.resize(s[0], s[1]);
                else
                    v.resize(s[1], s[0]);
            }
            else
            {
                v.resize(s[0]);
            }
        }
    };
} // namespace sciopp

#endif /* end of include guard: SCIOP_EIGEN_SHAPE_HPP */
