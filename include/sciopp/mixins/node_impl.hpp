#ifndef SCIOPP_MIXINS_NODE_IMPL_HPP
#define SCIOPP_MIXINS_NODE_IMPL_HPP
#include "sciopp/core/table.hpp"
#include "sciopp/hdf5/dataset.hpp"
#include "sciopp/hdf5/group.hpp"
#include "sciopp/hdf5/stream.hpp"

namespace sciopp
{
    template <class Self>
    template <class Atomic, size_t Rank>
    H5DataSet<Atomic, Rank> NodeMixin<Self>::create_dataset(const std::string& name,
                                                            const ShapeArray<Rank>& shape)
    {
        return H5DataSet<Atomic, Rank>::create(self().h5(), name, shape);
    }

    template <class Self>
    template <class Atomic, size_t Rank>
    H5DataSet<Atomic, Rank>
    NodeMixin<Self>::create_dataset(const std::string& name,
                                    const ShapeArray<Rank>& shape,
                                    const ShapeArray<Rank>& max_shape,
                                    const ShapeArray<Rank>& chunk_shape)
    {
        return H5DataSet<Atomic, Rank>::create(self().h5(), name, shape, max_shape,
                                               chunk_shape);
    }

    template <class Self>
    template <class Atomic, size_t Rank>
    H5DataSet<Atomic, Rank> NodeMixin<Self>::open_dataset(const std::string& name)
    {
        return H5DataSet<Atomic, Rank>::open(self().h5(), name);
    }

    template <class Self>
    template <class Atomic, size_t Rank>
    H5DataSet<Atomic, Rank> NodeMixin<Self>::get_dataset(const std::string& name)
    {
        return open_dataset<Atomic, Rank>(name);
    }

    template <class Self>
    template <class Atomic, size_t Rank>
    H5DataSet<Atomic, Rank>
    NodeMixin<Self>::open_or_create_dataset(const std::string& name,
                                            const ShapeArray<Rank>& shape,
                                            const ShapeArray<Rank>& max_shape,
                                            const ShapeArray<Rank>& chunk_shape)
    {
        if (exists(name))
        {
            return open_dataset(name);
        }

        return create_dataset(name, shape, max_shape, chunk_shape);
    }

    template <class Self>
    template <class Container>
    GenericDataSet<Container> NodeMixin<Self>::write_dataset(const std::string& name,
                                                             const Container& c)
    {
        auto s = ShapeHelper<Container>();
        auto d = GenericDataSet<Container>::create(self().h5(), name, s.shape(c));
        d.write(c);
        return d;
    }

    template <class Self>
    template <class Container>
    Container NodeMixin<Self>::read_dataset(const std::string& name)
    {
        auto d = GenericDataSet<Container>::open(self().h5(), name);
        /* compiler can't figure out that p.read_as is a template... */
        return d.template read_as<Container>();
    }

    template <class Self>
    template <class... Cols>
    auto NodeMixin<Self>::write_table(const std::string& name, Cols&&... cols)
    {
        using TableType = Table<GetAtomic<Cols>...>;
        TableType table;
        table.template insert_columns<0>(std::forward<Cols>(cols)...);

        auto d = H5DataSet<typename TableType::Atomic, 1>::create(
            self().h5(), name, table.shape(), table.dtype());

        d.write_buffer(table);
        return d;
    }

    template <class Self>
    template <class Atomic>
    H5Stream<Atomic> NodeMixin<Self>::create_stream(const std::string& name,
                                                    size_t buffer_size,
                                                    size_t chunk_size)
    {
        return H5Stream<Atomic>::create(self().h5(), name, buffer_size, chunk_size);
    }

    template <class Self>
    template <class Atomic>
    H5Stream<Atomic> NodeMixin<Self>::open_stream(const std::string& name,
                                                  size_t buffer_size)
    {
        return H5Stream<Atomic>::open(self().h5(), name, buffer_size);
    }

    template <class Self>
    template <class Atomic>
    H5Stream<Atomic> NodeMixin<Self>::open_or_create_stream(const std::string& name,
                                                            size_t buffer_size,
                                                            size_t chunk_size)
    {
        if (exists(name))
        {
            return open_stream<Atomic>(name, buffer_size);
        }
        return create_stream<Atomic>(name, buffer_size, chunk_size);
    }

    template <class Self>
    H5Group NodeMixin<Self>::create_group(const std::string& name)
    {
        return H5Group::create(self().h5(), name);
    }

    template <class Self>
    H5Group NodeMixin<Self>::open_group(const std::string& name)
    {
        return H5Group::open(self().h5(), name);
    }

    template <class Self>
    H5Group NodeMixin<Self>::get_group(const std::string& name)
    {
        return open_group(name);
    }

    template <class Self>
    std::vector<std::string> NodeMixin<Self>::groups()
    {
        auto ins = [](hid_t loc, const char* name, const H5L_info_t* info,
                      void* data) -> herr_t {
            auto child = H5Oopen(loc, name, H5P_DEFAULT);
            if (H5Iget_type(child) == H5I_GROUP)
            {
                auto names = reinterpret_cast<std::vector<std::string>*>(data);
                names->push_back(name);
            }
            return 0;
        };

        std::vector<std::string> names;
        H5Literate(self().h5().getId(), H5_INDEX_NAME, H5_ITER_INC, nullptr, ins, &names);

        return names;
    }

    template <class Self>
    std::vector<std::string> NodeMixin<Self>::datasets()
    {
        auto ins = [](hid_t loc, const char* name, const H5L_info_t* info,
                      void* data) -> herr_t {
            auto child = H5Oopen(loc, name, H5P_DEFAULT);
            if (H5Iget_type(child) == H5I_DATASET)
            {
                auto names = reinterpret_cast<std::vector<std::string>*>(data);
                names->push_back(name);
            }
            return 0;
        };

        std::vector<std::string> names;
        H5Literate(self().h5().getId(), H5_INDEX_NAME, H5_ITER_INC, nullptr, ins, &names);

        return names;
    }
} // namespace sciopp

#endif /* end of include guard: SCIOPP_MIXINS_NODE_IMPL_HPP */
