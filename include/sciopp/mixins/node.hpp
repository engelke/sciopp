#ifndef SCIOPP_MIXINS_NODE_HPP
#define SCIOPP_MIXINS_NODE_HPP
#include "sciopp/hdf5/declarations.hpp"

namespace sciopp
{
    template <class Self>
    class NodeMixin
    {
      public:
        inline bool exists(const std::string& name) const
        {
            return self().h5().exists(name);
        }

        template <class Atomic, size_t Rank>
        H5DataSet<Atomic, Rank> create_dataset(const std::string& name,
                                               const ShapeArray<Rank>& shape);

        template <class Atomic, size_t Rank>
        H5DataSet<Atomic, Rank> create_dataset(const std::string& name,
                                               const ShapeArray<Rank>& shape,
                                               const ShapeArray<Rank>& max_shape,
                                               const ShapeArray<Rank>& chunk_shape);

        template <class Atomic, size_t Rank>
        H5DataSet<Atomic, Rank> open_dataset(const std::string& name);

        template <class Atomic, size_t Rank>
        [[deprecated("use open_dataset instead")]] H5DataSet<Atomic, Rank>
        get_dataset(const std::string& name);

        template <class Atomic, size_t Rank>
        H5DataSet<Atomic, Rank>
        open_or_create_dataset(const std::string& name,
                               const ShapeArray<Rank>& shape,
                               const ShapeArray<Rank>& max_shape,
                               const ShapeArray<Rank>& chunk_shape);

        template <class Container>
        GenericDataSet<Container> write_dataset(const std::string& name,
                                                const Container& c);

        template <class Container>
        Container read_dataset(const std::string& name);

        template <class... Cols>
        auto write_table(const std::string& name, Cols&&... cols);

        template <class Atomic>
        H5Stream<Atomic> create_stream(const std::string& name,
                                       size_t buffer_size,
                                       size_t chunk_size = (4096 / sizeof(Atomic)));

        template <class Atomic>
        H5Stream<Atomic> open_stream(const std::string& name, size_t buffer_size);

        template <class Atomic>
        H5Stream<Atomic>
        open_or_create_stream(const std::string& name,
                              size_t buffer_size,
                              size_t chunk_size = (4096 / sizeof(Atomic)));


        H5Group create_group(const std::string& name);
        H5Group open_group(const std::string& name);

        [[deprecated("use open_group instead")]] H5Group
        get_group(const std::string& name);

        std::vector<std::string> groups();
        std::vector<std::string> datasets();

      private:
        Self& self() { return static_cast<Self&>(*this); }
        const Self& self() const { return static_cast<const Self&>(*this); }
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_MIXINS_NODE_HPP */
