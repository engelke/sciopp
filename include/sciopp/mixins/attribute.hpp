#ifndef SCIOPP_MIXINS_ATTRIBUTE_HPP
#define SCIOPP_MIXINS_ATTRIBUTE_HPP
#include "sciopp/core/buffer.hpp"
#include "sciopp/hdf5/exceptions.hpp"

namespace sciopp
{
    template <class T>
    void overwrite_attribute(H5::H5Object& obj, const std::string& name, const T& value)
    {
        /* remove attribute if it exists */
        if (obj.attrExists(name)) obj.removeAttr(name);

        auto buffer = H5ConstBuffer<T>(value);
        auto h5attr = obj.createAttribute(name, buffer.dtype(), buffer.dspace());
        h5attr.write(buffer.dtype(), buffer.data());
    }

    template <>
    inline void overwrite_attribute<std::string>(H5::H5Object& obj,
                                                 const std::string& name,
                                                 const std::string& value)
    {
        /* remove attribute if it exists */
        if (obj.attrExists(name)) obj.removeAttr(name);

        auto h5type = H5Type<std::string>();
        auto h5space = H5::DataSpace(H5S_SCALAR);
        auto h5attr = obj.createAttribute(name, h5type, h5space);

        h5attr.write(h5type, value);
    }

    template <class T>
    T read_attribute(const H5::H5Object& obj, const std::string& name)
    {
        T v;
        H5Buffer<T> b(v);
        H5::Attribute h5attr = obj.openAttribute(name);
        if constexpr (b.rank > 0)
        {
            auto fspace = h5attr.getSpace();
            typename H5Buffer<T>::Shape s;
            fspace.getSimpleExtentDims(s.data());

            if (!b.is_same_shape(s))
            {
                if constexpr (b.is_resizable)
                    b.reshape(s);
                else
                    throw IncompatibleShape();
            }
        }
        h5attr.read(b.dtype(), b.data());
        return v;
    }

    template <>
    inline std::string read_attribute<std::string>(const H5::H5Object& obj,
                                                   const std::string& name)
    {
        std::string str;
        H5::Attribute h5attr = obj.openAttribute(name);
        h5attr.read(H5Type<std::string>(), str);
        return str;
    }

    template <class Self>
    class AttributeMixin
    {
      public:
        /**
         * Set one attribute given name and value
         * @param name  Name of the attributes
         * @param value Value of the attribute
         */
        template <class T>
        void set_attribute(const std::string& name, const T& value)
        {
            overwrite_attribute(self().h5(), name, value);
        }

        /**
         * Read an attribute from the file and return it the user specified type T
         * @param name Name of the attribute
         */
        template <class T>
        T get_attribute(const std::string& name)
        {
            return read_attribute<T>(self().h5(), name);
        }

        /**
         * Check if attribute exists
         * @param  name Name of the attribute
         */
        bool has_attribute(const std::string& name)
        {
            return self().h5().attrExists(name);
        }

      private:
        Self& self() { return static_cast<Self&>(*this); }
        const Self& self() const { return static_cast<const Self&>(*this); }
    };
} // namespace sciopp

#endif /* end of include guard: SCIOPP_MIXINS_ATTRIBUTE_HPP */
