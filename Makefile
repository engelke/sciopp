CXX = g++
CXXFLAGS = -O0 -g -Wall -Wpedantic -std=c++17 -I./include

LD = g++
LDFLAGS = -lhdf5 -lhdf5_cpp -lfmt


# HDF5 LIBRARY LOCATION ###########################################
# Select serial version of hdf5 on debian if available
HDF5INC ?= /usr/include/hdf5/serial
ifneq ($(wildcard $(HDF5INC)),)
CXXFLAGS += -I$(HDF5INC)
endif

HDF5LIB ?= /usr/lib/x86_64-linux-gnu/hdf5/serial
ifneq ($(wildcard $(HDF5LIB)),)
LDFLAGS += -L$(HDF5LIB)
endif
# #################################################################

PREFIX ?= $(HOME)/.local

.PHONY: all library examples

all: examples
	echo $(LDFLAGS)


# LIBRARY #########################################################
install:
	cp -r include $(PREFIX)

clean:
	rm -r bin

# EXAMPLES ########################################################
EXAMPLE_CPP = $(shell find examples -name '*.cpp')
EXAMPLE_BIN = $(patsubst examples/%.cpp,bin/examples/%,$(EXAMPLE_CPP))

examples: $(EXAMPLE_BIN)

bin/examples/%: examples/%.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $< $(LDFLAGS) -o $@
