#include "sciopp/core.hpp"
#include "sciopp/eigen.hpp"


struct MyObservables
{
    int step;
    double E;
    Eigen::Vector3d cog;
};

SCIOPP_REGISTER_STRUCT(MyObservables, step, E, cog)

int main()
{
    /* Open a file, overwrite if it already exists */
    auto file = sciopp::H5File("/tmp/stream.h5", sciopp::H5File::Overwrite);

    /* Create a streamable dataset "obs" at file root. Set the internal buffer size of the
     * stream to 8 elements to reduce the amount of extend and write operations. */
    auto obs_stream = file.create_stream<MyObservables>("obs", 8);

    for (int i = 0; i < 5; ++i)
    {
        /* Create some data and push it to the stream */
        MyObservables obs{i, 1.1 * i, Eigen::Vector3d::Constant(0.1 * i)};
        obs_stream.push(obs);
    }

    /* usually the next step is unnecessary, because you read a stream from a file that
     * was already written. Here however, we have to flush obs_stream because after 5
     * elements the buffer was not full and no data has been written. obs_stream will
     * flush automatically, when it runs out of scope */
    obs_stream.flush();

    /* Reopen the stream. You can also change the buffer size */
    auto obs_stream2 = file.open_or_create_stream<MyObservables>("obs", 6);
    for (int i = 10; i < 15; ++i)
    {
        /* Create some data and push it to the stream */
        MyObservables obs{i, 1.1 * i, Eigen::Vector3d::Constant(0.1 * i)};
        obs_stream2.push(obs);
    }
}
