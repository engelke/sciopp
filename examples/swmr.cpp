#include "sciopp/core.hpp"

/* Ignore this =====================================*/
#include <chrono>
#include <thread>

#include <fmt/core.h>

#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>

termios orig_termios;
void reset_terminal_mode();
void set_conio_terminal_mode();
int kbhit();
int getch();
/*==================================================*/

int main(int argc, char* argv[])
{
    using namespace sciopp;

    /* Open file and create stream */
    auto file = H5File("/tmp/swmr.h5", H5File::Overwrite | H5File::SWMR);
    auto stream = file.create_stream<int>("iter", 1);


    /* Start loop */
    set_conio_terminal_mode();
    int i = 0;
    while (!kbhit())
    {
        /* add 1 number and write changes to disk */
        stream.push(i);
        file.flush();

        /* print iteration and sleep */
        fmt::print("\rIteration {:10} (press any key to exit)", i++);
        fflush(stdout);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    getch();
    reset_terminal_mode();

    fmt::print("\r\n");
}


/* Ignore this =====================================*/
void reset_terminal_mode() { tcsetattr(0, TCSANOW, &orig_termios); }

void set_conio_terminal_mode()
{
    termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}

int kbhit()
{
    struct timeval tv = {0L, 0L};
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv) > 0;
}

int getch()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0)
    {
        return r;
    }
    else
    {
        return c;
    }
}
