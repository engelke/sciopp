#include "sciopp/core.hpp"
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <numeric>

int main()
{
    using namespace sciopp;
    auto file = H5File("/tmp/vector.h5", H5File::Overwrite);
    file.set_attribute("foo", 1234);

    auto dset = file.create_dataset<int, 1>("dset", {8});
    dset.set_attribute("bar", 42);

    fmt::print("{}\n", file.get_attribute<int>("foo"));

    std::vector<int> v(8);
    std::iota(std::begin(v), std::end(v), 1);

    dset.write(v);

    file.write_dataset("dset2", v);

    file.create_group("grp");

    auto a = file.read_dataset<std::array<int, 8>>("dset2");
    fmt::print("{}\n", a);
}
