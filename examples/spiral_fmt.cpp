#include <cmath>
#include <cstdio>
#include <fmt/core.h>

struct Point
{
    static Point from_cyl(double r, double phi, double z)
    {
        return {r * cos(phi), r * sin(phi), z};
    }
    double x, y, z;
};

int main()
{
    auto out = fopen("/tmp/spiral_fmt.dat", "w");

    int n = 100000;
    /* create 5 spirals */
    double dphi = 4 * M_PI / n;
    double dz = 4. / n;
    for (int i = 0; i < 5; ++i)
    {
        double phi = i * 2 * M_PI / 5;
        double z = 0;
        for (int j = 0; j < n; ++j)
        {
            auto p = Point::from_cyl(1, phi, z);
            fmt::print(out, "{:0.14e} {:0.14e} {:0.14e}\n", p.x, p.y, p.z);
            phi += dphi;
            z += dz;
        }
    }

    fclose(out);
}
