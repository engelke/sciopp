#include <cmath>
#include <cstdio>
#include <fmt/core.h>
#include <fstream>
#include <iomanip>

struct Point
{
    static Point from_cyl(double r, double phi, double z)
    {
        return {r * cos(phi), r * sin(phi), z};
    }
    double x, y, z;
};

int main()
{
    std::ofstream out("/tmp/spiral_iostream.dat");
    out << std::setprecision(14) << std::scientific;

    int n = 100000;
    /* create 5 spirals */
    double dphi = 4 * M_PI / n;
    double dz = 4. / n;
    for (int i = 0; i < 5; ++i)
    {
        double phi = i * 2 * M_PI / 5;
        double z = 0;
        for (int j = 0; j < n; ++j)
        {
            auto p = Point::from_cyl(1, phi, z);
            // fmt::print(out, "{} {} {}\n", p.x, p.y, p.z);
            out << p.x << " " << p.y << " " << p.z << std::endl;
            phi += dphi;
            z += dz;
        }
    }

    // fclose(out);
}
