#include "sciopp/core.hpp"
#include <fmt/ranges.h>

int main()
{
    using namespace sciopp;
    auto file = H5File("/tmp/groups.h5", H5File::Overwrite);
    for (int i = 0; i < 10; ++i)
    {
        file.create_group(fmt::format("g{:02d}", i));
        file.create_dataset<double, 1>(fmt::format("d{:02d}", i), {4});
    }

    // std::vector<std::string> groups;
    // H5Literate(file.h5().getId(), H5_INDEX_NAME, H5_ITER_INC, nullptr, insert_groups,
    //            &groups);

    fmt::print("Groups: {}\n", file.groups());
    fmt::print("Datasets: {}\n", file.datasets());
}
