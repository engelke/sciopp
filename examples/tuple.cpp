#include "sciopp/core.hpp"
#include <fmt/ranges.h>

int main()
{
    using namespace sciopp;
    auto file = H5File("/tmp/tuple.h5", H5File::Overwrite);

    /* define a tuple and a container */
    using Particle = std::tuple<int, double, double>;
    std::vector<Particle> parts(8);

    /* create some data */
    int i = 0;
    for (auto& [id, x, y] : parts)
    {
        id = i++;
        x = 0.1 * id;
        y = -0.2 * id;
    }

    /* write a new dataset */
    file.write_dataset("particles", parts);

    /* read back */
    auto other_parts = file.read_dataset<std::vector<Particle>>("particles");

    /* and print */
    for (const auto& p : other_parts)
    {
        fmt::print("{}\n", p);
    }
}
