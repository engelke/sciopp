#include "sciopp/core.hpp"
#include <fmt/core.h>
#include <fmt/ranges.h>

int main()
{
    auto file = sciopp::H5File("/tmp/attr.h5", sciopp::H5File::Overwrite);

    /* Create a dataset */
    std::vector<double> v(8, 0.11);
    auto d = file.write_dataset("vec", v);

    /* Write one attribute */
    d.set_attribute("a", 0.123);

    if (d.has_attribute("a")) fmt::print("Attribute a exists\n");
    if (d.has_attribute("b")) fmt::print("Attribute b exists\n");

    // /* Write multiple attributes using the Attr class */
    // using sciopp::Attribute;
    // d.set_attributes(Attribute("b", 0.123), Attribute("c", 345));
    //
    /* Read attribute "a" */
    auto a = d.get_attribute<double>("a");
    fmt::print("a = {}\n", a);

    /* Read attribute "a" as a different type */
    auto a_int = d.get_attribute<int>("a");
    fmt::print("a_int = {}\n", a_int);

    /* Write a 1D Buffer */
    std::vector<double> arr{12.3, 34.5, 56.7, 78.9, 90.1};
    d.set_attribute("arr", arr);

    /* Read the previously written arr */
    auto vec = d.get_attribute<std::vector<double>>("arr");
    fmt::print("a = {}\n", vec);

    /* Update an existing attribute */
    arr.pop_back();
    d.set_attribute("arr", arr);

    /* Will throw IncompatibleShape because std::array cannot be resized */
    // d.get_attribute<std::array<int, 5>>("arr");

    /* However, this works fine */
    auto arr_int = d.get_attribute<std::array<int, 4>>("arr");
    fmt::print("a = {}\n", arr_int);

    using namespace std::string_literals;
    d.set_attribute("str", "a variable length string"s);
    auto str = d.get_attribute<std::string>("str");
    fmt::print("a = {}\n", str);
}
