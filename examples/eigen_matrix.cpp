#include "sciopp/core.hpp"
#include "sciopp/eigen.hpp"
#include <fmt/ranges.h>

int main()
{
    using namespace sciopp;
    auto file = H5File("/tmp/eigen_matrix.h5", sciopp::H5File::Overwrite);

    auto dset = file.create_dataset<int, 2>("matrix", {8, 8});

    Eigen::MatrixXi mat(3, 4);
    for (int i = 0; i < mat.rows(); ++i)
    {
        for (int j = 0; j < mat.cols(); ++j)
        {
            mat(i, j) = 10 * i + j;
        }
    }

    dset.write(mat, Stride<2>({2, 2}));

    Eigen::VectorXi r1 = mat.row(1);
    dset.write(r1, Offset<2>({2, 1}), Stride<2>({1, 1}), Reshape<2>({4, 1}));
    Eigen::VectorXi r2 = mat.row(2);
    dset.write(r2, Offset<2>({1, 2}), Stride<2>({1, 1}), Reshape<2>({1, 4}));

    /* Will create this file, because Eigen::MatrixXd are by default column major and
     * sciopp will reverse axes in this case */
    // 0, 0,  10, 0,  20, 0,  0, 0,
    // 0, 0,  20, 21, 22, 23, 0, 0
    // 1, 10, 11, 0,  21, 0,  0, 0,
    // 0, 11, 0,  0,  0,  0,  0, 0,
    // 2, 12, 12, 0,  22, 0,  0, 0,
    // 0, 13, 0,  0,  0,  0,  0, 0,
    // 3, 0,  13, 0,  23, 0,  0, 0,
    // 0, 0,  0,  0,  0,  0,  0, 0
}
