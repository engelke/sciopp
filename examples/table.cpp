#include "sciopp/core.hpp"
#include "sciopp/eigen.hpp"
#include <fmt/ranges.h>
#include <tuple>

using namespace sciopp;

int main()
{
    size_t n = 1 << 5;
    std::vector<int> ids(n);
    Eigen::VectorXd xs(n), ys(n), zs(n);

    for (size_t i = 0; i < n; ++i)
    {
        ids[i] = i;
        xs[i] = cos(2 * M_PI / n * i) + i * 1. / n;
        ys[i] = sin(2 * M_PI / n * i);
        zs[i] = sin(M_PI * ys[i]);
    }

    auto file = H5File("/tmp/table.h5", H5File::Overwrite);
    file.write_table("tab", Col("id", ids), Col("x", xs), Col("y", ys), Col("z", zs));
    //
    // auto grp = file.create_group("tab");
    // grp.write_dataset("id", ids);
    // grp.write_dataset("x", xs);
    // grp.write_dataset("y", ys);
    //
    // auto same_grp = file.get_group("tab");
    // same_grp.write_dataset("z", zs);
}
