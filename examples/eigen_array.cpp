#include "sciopp/core.hpp"
#include "sciopp/eigen.hpp"

int main()
{
    using namespace sciopp;
    auto file = H5File("/tmp/eigen_array.h5", H5File::Overwrite);

    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> arr(4, 8);
    arr.setConstant(42);

    file.write_dataset("arr", arr);
}
