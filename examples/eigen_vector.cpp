#include "sciopp/core.hpp"
#include "sciopp/eigen.hpp"
#include <fmt/core.h>
#include <fmt/ranges.h>


/* Just some function to see, what's in the container */
template <class Container>
void print(const std::string& var, Container& b)
{
    fmt::print("{} = [{:.6g}, shape=({})]\n", var,
               fmt::join(b.data(), b.data() + b.size(), ", "), b.size());
}

#define PRINT(X) print(#X, X)

int main()
{
    /* Open a file, overwrite it if already exists */
    auto file = sciopp::H5File("/tmp/eigen_vector.h5", sciopp::H5File::Overwrite);

    /* Writing data
     * ============ */

    /* Create a vector with dynamic size */
    Eigen::VectorXd v1 = Eigen::VectorXd::Constant(6, 1.1);
    PRINT(v1);
    /* and write v1 to a new dataset "v1" at the root of file. */
    auto d1 = file.write_dataset("v1", v1);

    /* Now create a vector with fixed size: */
    Eigen::Vector4d v2 = Eigen::Vector4d::Constant(2.1);
    PRINT(v2);
    /* This is the more verbose but also more flexible way of creating a dataset */
    auto d2 = file.create_dataset<double, 1>("v2", {4});
    /* Write v2 to d2 */
    d2.write(v2);

    /* Reading data
     * ============ */

    /* First the most convenient way */
    auto v3 = file.read_dataset<Eigen::ArrayXd>("v1");
    PRINT(v3);

    /* Or the slightly more complex way */
    auto d3 = file.open_dataset<double, 1>("v2");
    auto v4 = d3.read_as<Eigen::ArrayXd>();
    PRINT(v4);

    /* Or reusing an existing buffer */
    Eigen::ArrayXd v5;
    d3.read_to(v5);
    PRINT(v5);
}
