#include "sciopp/core.hpp"
#include <cmath>
#include <fmt/core.h>

struct Point
{
    static Point from_cyl(double r, double phi, double z)
    {
        return {r * cos(phi), r * sin(phi), z};
    }
    double x, y, z;
};
SCIOPP_REGISTER_STRUCT(Point, x, y, z)

int main()
{
    sciopp::H5File file("/tmp/spiral.h5", sciopp::H5File::Overwrite);

    int n = 100000;
    /* create 5 spirals */
    double dphi = 4 * M_PI / n;
    double dz = 4. / n;
    for (int i = 0; i < 5; ++i)
    {
        auto stream = file.create_stream<Point>(fmt::format("spiral{:d}", i), 512, 512);

        double phi = i * 2 * M_PI / 5;
        double z = 0;
        for (int j = 0; j < n; ++j)
        {
            stream.push(Point::from_cyl(1, phi, z));
            phi += dphi;
            z += dz;
        }
    }
}
